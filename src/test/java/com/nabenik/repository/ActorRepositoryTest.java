package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Inject
    MovieRepository movieRepository;

    Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");


    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(ActorRepository.class)
                .addClass(MovieRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");
    }

    @Test
    public void findById() {
        System.out.println("Find by id");
        movieRepository.create(movie);
        Actor actor = new Actor();
        actor.setMovie(this.movie);
        actor.setName("Actor");
        actorRepository.create(actor);
        Actor actor2=actorRepository.findById(actor.getActorId());
        assertEquals(actor.getActorId(),actor2.getActorId());
    }

    @Test
    public void create() {
        System.out.println("Create");
        movieRepository.create(movie);
        Actor actor = new Actor();
        actor.setMovie(this.movie);
        actor.setName("Actor");
        actorRepository.create(actor);
        assertNotNull(actor.getActorId());
    }

    @Test
    public void update() {
        System.out.println("Update");
        String name = "Actor Name";
        movieRepository.create(movie);
        Actor actor = new Actor();
        actor.setMovie(this.movie);
        actor.setName(name);
        actorRepository.create(actor);
        actor.setName("Actor New Name");
        Actor actorUpdate=actorRepository.update(actor);
        assertNotEquals(name,actorUpdate.getName());
    }

    @Test
    public void deleteById() {
        System.out.println("Delete by Id");
        movieRepository.create(movie);
        Actor actor = new Actor();
        actor.setMovie(this.movie);
        actor.setName("Name");
        actorRepository.create(actor);
        actorRepository.deleteById(actor.getActorId());
        Actor actor1=actorRepository.findById(actor.getActorId());
        assertNull(actor1);
    }

}
