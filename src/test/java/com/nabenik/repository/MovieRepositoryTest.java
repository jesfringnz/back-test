package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class MovieRepositoryTest {

    @Inject
    MovieRepository movieRepository;

    @Deployment
    public static WebArchive createDeployment(){
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(MovieRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");

        System.out.println(war.toString(true));

        return war;
    }

    @Test
    public void findById(){
        System.out.println("Find by id");
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movie);
        Movie movie2=movieRepository.findById(movie.getMovieId());
        assertEquals(movie.getTitle(),movie2.getTitle());
    }

    @Test
    public void update(){
        System.out.println("Update");
        String title = "El silencio de Jimmy";
        Movie movie = new Movie(title, "2014", "4 años");
        movieRepository.create(movie);
        movie.setTitle("Titulo cambiado");
        Movie movieUpdate=movieRepository.update(movie);
        assertNotEquals(title,movieUpdate.getTitle());
    }

    @Test
    public void deleteById(){
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movie);
        movieRepository.deleteById(movie.getMovieId());
        Movie movie1=movieRepository.findById(movie.getMovieId());
        assertNull(movie1);
    }

    @Test
    public void listAll(){
        String commonTitle="Titulo";
        int moviesCreated = 1;
        ArrayList<Movie> movies =createMovies(commonTitle,moviesCreated);
        System.out.println("Peliculas creadas:"+movies.size());
        for (Movie m:
             movies) {
            movieRepository.create(m);
            System.out.println(m.getMovieId());
        }
        int foundMovies=movieRepository.listAll(commonTitle).size();
        assertEquals(moviesCreated,foundMovies);

    }

    @Test
    public void create() {
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        movieRepository.create(movie);


        System.out.println("Movie Id " + movie.getMovieId());

        assertNotNull(movie.getMovieId());
    }

    public ArrayList<Movie> createMovies(String commonTitle,int number){
        ArrayList<Movie> peliculas = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            peliculas.add(new Movie(commonTitle+" "+i,"2020","3"));
        }
        return peliculas;
    }

}
