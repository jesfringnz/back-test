# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* JavaScript knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations

JSON-P:
>JSON Processing (JSON-P) is a Java API for processing (e.g. parsing, generating, transforming and querying) JSON messages. It has been around since earlier versions of JavaEE 8 and was updated to include support for JSON Pointer and JSON Patch,  to allow manipulations of JSON directly on text and without Marshalling to a Java object.

>Implementations:JSON-P Object Model calls and JSON-P Streaming API calls and https://javaee.github.io/jsonp/

CDI: 
>CDI (Contexts and Dependency Injection) is a standard dependency injection framework included in Java EE 6 and higher. It allows us to manage the lifecycle of stateful components via domain-specific lifecycle contexts and inject components (services) into client objects in a type-safe way.

>Implementations: JBoss Weld:http://weld.cdi-spec.org/
                 Apache Open Webans: http://openwebbeans.apache.org/

JPA: 
>Java Persistence API is a source-to-store business entities as relational entities. It shows how to define a PLAIN OLD JAVA OBJECT (POJO) as an entity and how to manage entities with relations.

>Implementations: Hibernate: https://hibernate.org/
                 EclipseLink: https://www.eclipse.org/eclipselink/


JAX-RS: 
>JAX-RS stands for JAVA API for RESTful Web Services. JAX-RS is a JAVA based programming language API and specification to provide support for created RESTful Web Services.

>Implementations: RESTEasy:https://resteasy.github.io/
                 Apache CXF: http://cxf.apache.org/



3. Which of the following is/are not an application server?

* Helidon
* Quarkus
* WebSphere
* Tomcat
* KumuluzEE

>Helidon,Quarkus,Tomcat and KumuluzEE are not an application server

4. In your opinion what's the main benefit of moving from Java 8 to Java 11

>Improved performance and memory, as well as new functionalities

5. In your opinion what's the main benefit of using TypeScript over JavaScript

>Mainly static typing, and then typescrypt is compiled to javascript.

6. What's the main difference between OpenJDK and HotSpot

>In functional terms, there is little difference between a Hotspot release and an OpenJDK release. There are some extra "enterprise" features in Hotspot that Oracle (paying) Java customers can enable, but apart from that the same technology are present in both Hotspot and OpenJDK.

7. If no database is configured? Will you be able to run this project? Why?

>The reason is that it uses the database of the application server where it runs, in this case h2, which is the payara embedded database.

## Development tasks

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11 and  NodeJS 16
![](Capturas/img.png)
1. (easy) Build this project on a regular CLI over OpenJDK 11
   ![](Capturas/img_1.png)

2. (easy) Run this project using an IDE/Editor of your choice
![](Capturas/ide.png)
3. (medium) This project has been created using Java EE APIs, please identify at least four APIs, bump it to Java EE 8 and Java 11, later run it over regular Payara Application Server 
APIs:JPA ,EJB ,Bean Validation
![](Capturas/payara.png)
4. (medium) Execute the movie endpoint operations using a client -e.g. Postman-, if needed please also check/fix the code to be REST compliant

-Create movie
![](Capturas/createMovie.png)

-List movie
![](Capturas/list.png)

-findById 
![](Capturas/find.png)

-Update
![](Capturas/update.png)

-Delete
![](Capturas/delete.png)

5. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer

-Repo SPA: https://bitbucket.org/jesfringnz/spa-nabenik/src/master/

![](Capturas/spa.png)

7. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods
![](Capturas/movieTest.png)

8. (hard) Based on `MovieRepository` Integration Test, please create an integration test using [RestAssured](https://rest-assured.io/) and `MovieController`

9. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test
![](Capturas/actorTest.png)
10. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Oracle Helidon](https://helidon.io/). Do it and don't port Integration Tests

